﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeledocTest.Data.Entities;
using TeledocTest.Data.Factories;
using TeledocTest.Data.Settings;

namespace TeledocTest.Data
{
    public class Context : DbContext
    {
        public Context() : base()
        {

        }

        public DbSet<Founder> Founders { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<FounderOrganization> FounderOrganizations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(ConnectionFactory.ConnectionToDb);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new FounderSettings());
            modelBuilder.ApplyConfiguration(new OrganizationSettings());
            modelBuilder.ApplyConfiguration(new FounderOrganizationSettings());

            Seed(modelBuilder);
        }
        protected void Seed(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Founder>().HasData(
                new Founder() { FirstName = "Иван", LastName = "Иванов", MiddleName = "Иванович", Id = new Guid("c6161fd3-29c5-4559-87e7-e986224df43a") },
                new Founder() { FirstName = "Алексей", LastName = "Алексеев", MiddleName = "Алексеевич", Id = new Guid("f2bfb967-c300-4415-9c78-6ec4ef227706") },
                new Founder() { FirstName = "Прокопенко", LastName = "Прокопий", MiddleName = "Прокопеевмч", Id = new Guid("784e5c1c-324e-422c-91ee-370502410090") }
            );
            modelBuilder.Entity<Organization>().HasData(
                new Organization() { Name = "ИП Альфа", INN = "0123456789", Id = new Guid("c69436c7-e0ed-4094-b89a-7c2f99269c9f") },
                new Organization() { Name = "ИП Бета", INN = "012345678910", Id = new Guid("40cd0b2c-8270-4a4e-8b6b-fd3e74a99040") }
                );
            modelBuilder.Entity<FounderOrganization>().HasData(
                new FounderOrganization() { FounderId = new Guid("784e5c1c-324e-422c-91ee-370502410090"), OrganizationId = new Guid("c69436c7-e0ed-4094-b89a-7c2f99269c9f") },
                new FounderOrganization() { FounderId = new Guid("c6161fd3-29c5-4559-87e7-e986224df43a"), OrganizationId = new Guid("c69436c7-e0ed-4094-b89a-7c2f99269c9f") },
                new FounderOrganization() { FounderId = new Guid("f2bfb967-c300-4415-9c78-6ec4ef227706"), OrganizationId = new Guid("c69436c7-e0ed-4094-b89a-7c2f99269c9f") },
                new FounderOrganization() { FounderId = new Guid("784e5c1c-324e-422c-91ee-370502410090"), OrganizationId = new Guid("40cd0b2c-8270-4a4e-8b6b-fd3e74a99040") }

                );
        }

    }
}
