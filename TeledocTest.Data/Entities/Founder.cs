﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeledocTest.Data.Entities
{
    public class Founder
    {
        public Founder()
        {
            Id = Guid.NewGuid();
            Organizations = new List<FounderOrganization>();
        }

        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }

        public ICollection<FounderOrganization> Organizations { get; set; }
        
    }
}
