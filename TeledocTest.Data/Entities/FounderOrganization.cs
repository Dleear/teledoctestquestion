﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeledocTest.Data.Entities
{
    public class FounderOrganization
    {
        public FounderOrganization()
        {

        }

        public Guid FounderId { get; set; }
        public Guid OrganizationId { get; set; }
        public Founder Founder { get; set; }
        public Organization Organization { get; set; }
    }
}
