﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeledocTest.Data.Entities
{
    public class Organization
    {
        public Organization()
        {
            Id = Guid.NewGuid();
            Founders = new List<FounderOrganization>();
            DateOpen = DateTime.Now;
        }

        public Guid Id { get; set;}
        public string Name { get; set; }
        public string INN { get; set; }
        public DateTime DateOpen { get; set; }
        public DateTime? DateClosed { get; set; }
        public  ICollection<FounderOrganization> Founders { get; set; }

    }
}
