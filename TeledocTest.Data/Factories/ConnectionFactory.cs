﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeledocTest.Data.Factories
{
    public class ConnectionFactory
    {
        public static string ConnectionToDb { get; }

        static ConnectionFactory()
        {
            var sb = new SqlConnectionStringBuilder()
            {
                DataSource = @"(LocalDB)\MSSQLLocalDB",
                InitialCatalog = "OrganizationsTestDatabase",
                IntegratedSecurity = true
            };
            ConnectionToDb = sb.ConnectionString;
        }
    }
}
