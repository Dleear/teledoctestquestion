﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeledocTest.Data.Entities;

namespace TeledocTest.Data.Settings
{
    public class FounderOrganizationSettings : IEntityTypeConfiguration<FounderOrganization>
    {
        public void Configure(EntityTypeBuilder<FounderOrganization> builder)
        {
            builder.HasKey(p => new { p.FounderId, p.OrganizationId});
        }
    }
}
