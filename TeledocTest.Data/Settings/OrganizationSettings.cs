﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeledocTest.Data.Entities;

namespace TeledocTest.Data.Settings
{
    public class OrganizationSettings:IEntityTypeConfiguration<Organization>
    {
        public void Configure(EntityTypeBuilder<Organization> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.INN).IsRequired().HasMaxLength(12);
            builder.Property(p => p.Name).IsRequired();
        }
    }
}
