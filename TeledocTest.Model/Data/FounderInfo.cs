﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeledocTest.Model.Data
{
    public sealed class FounderInfo
    {
        public Guid Id { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public string MiddleName { get; }
        public ICollection<OrganizationInfo> Organizations { get; }

        public FounderInfo(Guid id, string name, string lastName, string middleName) : this(id, name, lastName, middleName, null)
        {
        }
        public FounderInfo(Guid id, string name, string lastName, string middleName, ICollection<OrganizationInfo> organizations)
        {
            Id = id;
            FirstName = name;
            LastName = lastName;
            MiddleName = middleName;
            Organizations = organizations;
        }

    }
}
