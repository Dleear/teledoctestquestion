﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeledocTest.Model.Data
{
    public sealed class OrganizationInfo
    {
        public Guid Id { get; }
        public string Name { get; }
        public string INN { get; }
        public DateTime DateOpen { get; }
        public ICollection<FounderInfo> Founders { get; }

        public OrganizationInfo(Guid id, string name, string inn, DateTime dateOpen) : this(id, name, inn, dateOpen, null)
        {
        }

        public OrganizationInfo(Guid id, string name, string inn, DateTime dateOpen, ICollection<FounderInfo> founders)
        {
            Name = name;
            INN = inn;
            DateOpen = dateOpen;
            Founders = founders;
        }

    }
}
