﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeledocTest.Data.Entities;
using TeledocTest.Model.Data;
using Microsoft.EntityFrameworkCore;
namespace TeledocTest.Model.Extentions
{
    public static class FounderExtentions
    {

        public static FounderInfo ToInfo(this Founder it)
        {
            var organizations = it.Organizations
                .Select(org => new OrganizationInfo(org.Organization.Id, org.Organization.Name, org.Organization.INN, org.Organization.DateOpen))
                .ToList();
            return new FounderInfo(it.Id, it.FirstName, it.LastName, it.MiddleName, organizations);
        }
    }
}
