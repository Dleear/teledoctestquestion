﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TeledocTest.Data.Entities;
using TeledocTest.Model.Data;

namespace TeledocTest.Model.Extentions
{
    public static class OrganizationExtentions
    {
        public static bool IsValidInn(this string inn)
        {
            var regex = new Regex("(^[0-9]{10}$)|(^[0-9]{12}$)");
            return regex.IsMatch(inn);
        }
        public static IEnumerable<Organization> GetActiveOrganization(this IEnumerable<Organization> organizations)
        {
            return organizations.Where(x => x.DateClosed == null);
        }

        public static OrganizationInfo ToInfo(this Organization organization)
        {
            var founders = organization.Founders
                .Select(fo => new FounderInfo(fo.Founder.Id, fo.Founder.FirstName, fo.Founder.LastName, fo.Founder.MiddleName))
                .ToList();
            return new OrganizationInfo(organization.Id, organization.Name, organization.INN, organization.DateOpen, founders);
        }

    }
}
