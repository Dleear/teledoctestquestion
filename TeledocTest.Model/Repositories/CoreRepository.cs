﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using System.Threading.Tasks;
using TeledocTest.Data;
using TeledocTest.Data.Factories;

namespace TeledocTest.Model.Repositories
{
    public abstract class CoreRepository
    {
        protected void ActionWithContext(Action<Context> act)
        {
            using(Context context = ContextFactory.BuildContext())
            {
                act(context);
            }
        }
        protected void TransaktionWithContext(Action<Context> act)
        {
            using(Context contex = ContextFactory.BuildContext())
            {
                act(contex);
                contex.SaveChanges();
            }
        }
        protected T FuncWithContext<T>(Func<Context,T> func)
        {
            using(Context context = ContextFactory.BuildContext())
            {
                T result =  func(context);
                return result;
            }
        }
        
    }
}
