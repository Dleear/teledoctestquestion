﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeledocTest.Data.Entities;
using TeledocTest.Model.Data;
using TeledocTest.Model.Extentions;

namespace TeledocTest.Model.Repositories
{
    public class OrganizationRepository : CoreRepository
    {
        public IEnumerable<OrganizationInfo> GetAllOrganizations()
        {
            return FuncWithContext(c => c.Organizations
                .Include(org => org.Founders)
                .ThenInclude(f => f.Founder)
                .GetActiveOrganization()
                .Select(org => org.ToInfo())
                .OrderBy(x => x.DateOpen)
                .ToList()
            );
        }
        public IEnumerable<FounderInfo> GetAllFounders()
        {
            return FuncWithContext(c => c.Founders
            .Include(x => x.Organizations)
            .ThenInclude(x => x.Organization)
            .Select(x => x.ToInfo())
            .ToList()
            .OrderBy(x => x.FirstName)

            );
        }
        public OrganizationInfo GetOrganization(Guid id)
        {
            return FuncWithContext(c => c.Organizations.FirstOrDefault(x => x.Id == id)?.ToInfo());
        }
        public Guid CreateFounder(FounderInfo founder)
        {
            var newFounder = new Founder()
            {
                FirstName = founder.FirstName,
                LastName = founder.LastName,
                MiddleName = founder.MiddleName
            };
            TransaktionWithContext(c => c.Founders.Add(newFounder));
            return newFounder.Id;
        }

        public void CreateOrganization(string name, string inn, IEnumerable<Guid> founders)
        {
            TransaktionWithContext(c =>
            {
                if (!inn.IsValidInn()) throw new ArgumentException("Такого ИНН не может быть. ИНН должен состоять из цифр в количестве 10 или 12");
                if (!founders.Any()) throw new ArgumentException("У организации должен быть хотя бы один учредитель");
                if (c.Organizations.Any(org => org.Name == name)) throw new ArgumentException("Организация с таким названием уже существует");
                if (c.Organizations.Any(org => org.INN == inn)) throw new ArgumentException("Такой ИНН уже зарегистрирован");

                var foundedFounders = c.Founders.Where(f => founders.Contains(f.Id));
                if (foundedFounders.Count() != founders.Count())
                    throw new ArgumentException("Подозрительные учредители - не все из учредителей найдены в базе, пожалуйста, исправьте эту оплошность");

                var newOrganization = new Organization()
                {
                    Name = name,
                    INN = inn,
                };
                c.Organizations.Add(newOrganization);
                c.FounderOrganizations.AddRange(foundedFounders.Select(f => new FounderOrganization() { Founder = f, Organization = newOrganization }));

            });
        }
    }
}
