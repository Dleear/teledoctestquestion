﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TeledocTest.Model.Repositories;

namespace TeledocTest.Controllers
{
    public class BaseController : Controller
    {
        protected OrganizationRepository repository;

        public BaseController(OrganizationRepository repository)
        {
            this.repository = repository;
        }

        protected IActionResult TryCatch(Func<IActionResult> act)
        {
            try
            {
                return act();
            }
            catch (Exception ex)
            {
                HttpContext.Response.StatusCode = 500;
                return Json(ex.Message);
            }
        }
    }
}