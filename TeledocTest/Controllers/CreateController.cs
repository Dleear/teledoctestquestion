﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TeledocTest.Model;
using TeledocTest.Model.Data;
using TeledocTest.Model.Repositories;

namespace TeledocTest.Controllers
{
    [Route("Create")]
    public class CreateController : BaseController
    {
        public CreateController(OrganizationRepository repository) : base(repository)
        {
        }
        public IActionResult Index()
        {
            return TryCatch(() => View(repository.GetAllFounders()));
        }

        [Route("Founder")]
        [HttpPost]
        public IActionResult CreateFounder([FromBody] NewFounder founder)
        {
            return TryCatch(() =>
            {
                if (!ModelState.IsValid) throw new ArgumentException("Не все поля заполнены");
                var result = repository.CreateFounder(new FounderInfo(Guid.Empty, founder.FirstName, founder.LastName, founder.MiddleName));
                return Ok(result);
            });
        }

        [Route("Organization")]
        [HttpPost]
        public IActionResult CreateOrganization([FromBody] NewOrganization organization )
        {
            return TryCatch(() => {
                if (!ModelState.IsValid) throw new ArgumentException("Не все поля заполнены");
                var guids = organization.Founders.Select(x => new Guid(x));
                repository.CreateOrganization(organization.Name, organization.Inn, guids);
                return Ok("Сохранено");
            });
        }
    }
}