﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TeledocTest.Model.Data;
using TeledocTest.Model.Repositories;

namespace TeledocTest.Controllers
{
    [Route("Index")]
    public class IndexController : BaseController
    {
        public IndexController(OrganizationRepository repository) : base(repository)
        {
        }

        public IActionResult Index()
        {
            return TryCatch(() => View(repository.GetAllOrganizations()));
        }
    }
}