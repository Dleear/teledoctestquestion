﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TeledocTest.Model
{
    public class NewOrganization
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Inn { get; set; }
        [Required]
        public string[] Founders { get; set; }
    }
}
