﻿'use strict';


function ShowError(error) {
    var errorBox = document.getElementById('errorContainer');
    errorBox.innerText = error;
    setTimeout(function () {
        errorBox.innerText = '';
       
    },6000)
}
function HandleFetchError(responce) {
    if (!responce.ok) {
        responce.json().then(r=>ShowError(r));
        throw Error();
    }
    return responce;
}

const CreateFounderRoute = "/Create/Founder";
const CreateOrganizationRoute = "/Create/Organization"