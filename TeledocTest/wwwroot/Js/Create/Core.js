﻿'use strict';

let select = document.getElementById('existing-founder__select');
let addBtn = document.getElementById('existing-founder__add');
let createBtn = document.getElementById('create-new-founder--btn');
let saveBtn = document.getElementById('form-container__save');
let listView = document.getElementById('form-container__founders');
let nameBox = document.getElementById('form-container__name')
let innBox = document.getElementById('form-container__inn');

//create new organization btn click
saveBtn.addEventListener('click', e => {
    e.preventDefault();


    if (nameBox.value.length === 0) {
        ShowError("Имя обязательно должно быть заполнено");
        return
    }
    if (innBox.value.length != 10 && innBox.value.length != 12) {
        
        ShowError("Инн должен состоять из 10 или 12 цифр");
        return;
    }
    let founders = Array.from(listView.children);
    if (founders.length == 0) {
        ShowError("Должен быть хотя бы один учредитель");
        return;
    }
   
    let data = {
        Name: nameBox.value,
        Inn: innBox.value,
        Founders:founders.map(el=>el.dataset.id)
    }
    console.log(data);
    console.log(JSON.stringify(data));
  
    fetch(CreateOrganizationRoute, { method: 'POST', headers: { 'Content-Type': 'application/json', }, body: JSON.stringify(data) })
        .then(HandleFetchError)
        .then(r => document.location = '/Index/');
        
    


});

// check inn valid
innBox.addEventListener('keyup', e => {
    let inn = innBox.value;
    inn = inn.split('').filter(ch => !isNaN(ch) && ch!==' ');
    if (inn.length > 12) inn = inn.slice(0, 12);
    innBox.value = inn.join('');
});

//Attach founder to new organization btn click
addBtn.addEventListener('click', function (ev) {
    ev.preventDefault();
    let item = select.options[select.selectedIndex];
    if (Array.from(listView.children).find((el, i, arr) => el.dataset.id == item.value) != null) {
        ShowError("Такой Учредитель уже добавлен");
        return;
    }
    appendFounder(item.innerHTML, item.value);

});

//Create new founder btn click
createBtn.addEventListener('click', e => {
    e.preventDefault();
    let data = {
        LastName: document.getElementById('create-new-founder--lastName').value,
        FirstName: document.getElementById('create-new-founder--firstName').value,
        MiddleName: document.getElementById('create-new-founder--middleName').value
    };
    let fullName = `${data.LastName} ${data.FirstName} ${data.MiddleName}`
    fetch(CreateFounderRoute, { method: 'POST', headers: { 'Content-Type': 'application/json', }, body: JSON.stringify(data) })
        .then(r => HandleFetchError(r))
        .then(r => r.json())
        .then(r => appendFounder(fullName, r));
});

function appendFounder(name, id) {
    let founderToAppend = document.createElement('div');
    founderToAppend.innerHTML = name;
    founderToAppend.dataset.id = id;
    listView.appendChild(founderToAppend);
}

